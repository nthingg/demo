package com.madcolors.web.rest;

import com.madcolors.domain.Customer;
import com.madcolors.repository.CustomerRepository;
import com.madcolors.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.madcolors.domain.Customer}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CustomerResource {

    private final Logger log = LoggerFactory.getLogger(CustomerResource.class);

    private static final String ENTITY_NAME = "customer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerRepository customerRepository;

    public CustomerResource(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    /**
     * {@code POST  /customers} : Create a new customer.
     *
     * @param customer the customer to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customer, or with status {@code 400 (Bad Request)} if the customer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customers")
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer) throws URISyntaxException {
        log.debug("REST request to save Customer : {}", customer);
        if (customer.getId() != null) {
            throw new BadRequestAlertException("A new customer cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (customerRepository.existsByTelephone(customer.getTelephone())) {
            throw new BadRequestAlertException("Customer Registration fail", ENTITY_NAME, "telephone already exist");
        }

        Customer result = customerRepository.save(customer);
        return ResponseEntity
            .created(new URI("/api/customers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customers/:id} : Updates an existing customer.
     *
     * @param id the id of the customer to save.
     * @param customer the customer to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customer,
     * or with status {@code 400 (Bad Request)} if the customer is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customer couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customers/{id}")
    public ResponseEntity<Customer> updateCustomer(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Customer customer
    ) throws URISyntaxException {
        log.debug("REST request to update Customer : {}, {}", id, customer);
        if (customer.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, customer.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!customerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Customer result = customerRepository.save(customer);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customer.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /customers/:id} : Partial updates given fields of an existing customer, field will ignore if it is null
     *
     * @param id the id of the customer to save.
     * @param customer the customer to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customer,
     * or with status {@code 400 (Bad Request)} if the customer is not valid,
     * or with status {@code 404 (Not Found)} if the customer is not found,
     * or with status {@code 500 (Internal Server Error)} if the customer couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/customers")
    public ResponseEntity<Customer> partialUpdateCustomer( @NotNull @RequestBody Customer customer) throws URISyntaxException {
        log.debug("REST request to partial update Customer partially : {}, {}", customer.getTelephone(), customer);

        if (customer.getTelephone() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "phonenull");
        }

        if (!customerRepository.existsByTelephone(customer.getTelephone())) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "phonenotfound");
        }

        Optional<Customer> result = customerRepository
            .findCustomerByTelephone(customer.getTelephone())
            .map(
                existingCustomer -> {
                    if (customer.getTelephone() != null) {
                        existingCustomer.setTelephone(customer.getTelephone());
                    }
                    if (customer.getFullName() != null) {
                        existingCustomer.setFullName(customer.getFullName());
                    }
                    if (customer.getEmail() != null) {
                        existingCustomer.setEmail(customer.getEmail());
                    }
                    if (customer.getAddress() != null) {
                        existingCustomer.setAddress(customer.getAddress());
                    }
                    if (customer.getRankPoint() != null) {
                        throw new BadRequestAlertException("Update fail", ENTITY_NAME, "cant update rank point");
                    }
                    if (customer.getCreatedAt() != null) {
                        throw new BadRequestAlertException("Update fail", ENTITY_NAME, "cant update create date");
                    }
                    if (customer.getId() != null) {
                        throw new BadRequestAlertException("Update fail", ENTITY_NAME, "cant update id");
                    }

                    return existingCustomer;
                }
            )
            .map(customerRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customer.getTelephone())
        );
    }

    /**
     * {@code GET  /customers} : get all the customers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customers in body.
     */
    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        log.debug("REST request to get all Customers");
        return customerRepository.findAll();
    }

    /**
     * {@code GET  /customers/:id} : get the "id" customer.
     *
     * @param id the id of the customer to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customer, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/customers/{id}")
//    public ResponseEntity<Customer> getCustomer(@PathVariable Long id) {
//        log.debug("REST request to get Customer : {}", id);
//        Optional<Customer> customer = customerRepository.findById(id);
//        return ResponseUtil.wrapOrNotFound(customer);
//    }

    @GetMapping("/customers/{telephone}")
    public ResponseEntity<Customer> getCustomerByTelephone(@PathVariable String telephone) {
        log.debug("REST request to get Customer by telephone: {}", telephone);
        Optional<Customer> customer = customerRepository.findCustomerByTelephone(telephone);
        return ResponseUtil.wrapOrNotFound(customer);
    }


    /**
     * {@code DELETE  /customers/:id} : delete the "id" customer.
     *
     * @param id the id of the customer to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {
        log.debug("REST request to delete Customer : {}", id);
        customerRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
