package com.madcolors.web.rest;

import com.madcolors.domain.RefundedItem;
import com.madcolors.repository.RefundedItemRepository;
import com.madcolors.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.madcolors.domain.RefundedItem}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class RefundedItemResource {

    private final Logger log = LoggerFactory.getLogger(RefundedItemResource.class);

    private static final String ENTITY_NAME = "refundedItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RefundedItemRepository refundedItemRepository;

    public RefundedItemResource(RefundedItemRepository refundedItemRepository) {
        this.refundedItemRepository = refundedItemRepository;
    }

    /**
     * {@code POST  /refunded-items} : Create a new refundedItem.
     *
     * @param refundedItem the refundedItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new refundedItem, or with status {@code 400 (Bad Request)} if the refundedItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/refunded-items")
    public ResponseEntity<RefundedItem> createRefundedItem(@Valid @RequestBody RefundedItem refundedItem) throws URISyntaxException {
        log.debug("REST request to save RefundedItem : {}", refundedItem);
        if (refundedItem.getId() != null) {
            throw new BadRequestAlertException("A new refundedItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RefundedItem result = refundedItemRepository.save(refundedItem);
        return ResponseEntity
            .created(new URI("/api/refunded-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /refunded-items/:id} : Updates an existing refundedItem.
     *
     * @param id the id of the refundedItem to save.
     * @param refundedItem the refundedItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refundedItem,
     * or with status {@code 400 (Bad Request)} if the refundedItem is not valid,
     * or with status {@code 500 (Internal Server Error)} if the refundedItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/refunded-items/{id}")
    public ResponseEntity<RefundedItem> updateRefundedItem(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody RefundedItem refundedItem
    ) throws URISyntaxException {
        log.debug("REST request to update RefundedItem : {}, {}", id, refundedItem);
        if (refundedItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refundedItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refundedItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RefundedItem result = refundedItemRepository.save(refundedItem);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refundedItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /refunded-items/:id} : Partial updates given fields of an existing refundedItem, field will ignore if it is null
     *
     * @param id the id of the refundedItem to save.
     * @param refundedItem the refundedItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated refundedItem,
     * or with status {@code 400 (Bad Request)} if the refundedItem is not valid,
     * or with status {@code 404 (Not Found)} if the refundedItem is not found,
     * or with status {@code 500 (Internal Server Error)} if the refundedItem couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/refunded-items/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<RefundedItem> partialUpdateRefundedItem(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody RefundedItem refundedItem
    ) throws URISyntaxException {
        log.debug("REST request to partial update RefundedItem partially : {}, {}", id, refundedItem);
        if (refundedItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, refundedItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!refundedItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RefundedItem> result = refundedItemRepository
            .findById(refundedItem.getId())
            .map(
                existingRefundedItem -> {
                    if (refundedItem.getReason() != null) {
                        existingRefundedItem.setReason(refundedItem.getReason());
                    }
                    if (refundedItem.getRefundPrice() != null) {
                        existingRefundedItem.setRefundPrice(refundedItem.getRefundPrice());
                    }
                    if (refundedItem.getCreatedAt() != null) {
                        existingRefundedItem.setCreatedAt(refundedItem.getCreatedAt());
                    }

                    return existingRefundedItem;
                }
            )
            .map(refundedItemRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, refundedItem.getId().toString())
        );
    }

    /**
     * {@code GET  /refunded-items} : get all the refundedItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of refundedItems in body.
     */
    @GetMapping("/refunded-items")
    public List<RefundedItem> getAllRefundedItems() {
        log.debug("REST request to get all RefundedItems");
        return refundedItemRepository.findAll();
    }

    /**
     * {@code GET  /refunded-items/:id} : get the "id" refundedItem.
     *
     * @param id the id of the refundedItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the refundedItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/refunded-items/{id}")
    public ResponseEntity<RefundedItem> getRefundedItem(@PathVariable Long id) {
        log.debug("REST request to get RefundedItem : {}", id);
        Optional<RefundedItem> refundedItem = refundedItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(refundedItem);
    }

    /**
     * {@code DELETE  /refunded-items/:id} : delete the "id" refundedItem.
     *
     * @param id the id of the refundedItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/refunded-items/{id}")
    public ResponseEntity<Void> deleteRefundedItem(@PathVariable Long id) {
        log.debug("REST request to delete RefundedItem : {}", id);
        refundedItemRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
