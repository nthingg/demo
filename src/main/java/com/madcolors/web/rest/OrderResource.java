package com.madcolors.web.rest;

import com.madcolors.domain.*;
import com.madcolors.repository.*;
import com.madcolors.service.UserService;
import com.madcolors.web.rest.errors.BadRequestAlertException;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.madcolors.web.rest.vm.OrderVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;

/**
 * REST controller for managing {@link com.madcolors.domain.Order}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class OrderResource {

    private final Logger log = LoggerFactory.getLogger(OrderResource.class);

    private static final String ENTITY_NAME = "order";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final UserService userService;
    private final PaymentMethodRepository paymentMethodRepository;
    private final CustomerRepository customerRepository;
    private final RankRepository rankRepository;
    private final OrderItemRepository orderItemRepository;
    private final RefundedItemRepository refundedItemRepository;

    public OrderResource(OrderRepository orderRepository, ProductRepository productRepository, UserService userService,
                         PaymentMethodRepository paymentMethodRepository,
                         CustomerRepository customerRepository,
                         RankRepository rankRepository,
                         OrderItemRepository orderItemRepository,
                         RefundedItemRepository refundedItemRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.userService = userService;
        this.paymentMethodRepository = paymentMethodRepository;
        this.customerRepository = customerRepository;
        this.rankRepository = rankRepository;
        this.orderItemRepository = orderItemRepository;
        this.refundedItemRepository = refundedItemRepository;
    }

    /**
     * {@code POST  /orders} : Create a new order.
     *
     * @param order the order to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new order, or with status {@code 400 (Bad Request)} if the order has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    private String getRandomOrderCode() {
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd"));
        int leftLimit = 65;
        int rightLimit = 90;
        int targetStringLength = 4;
        Random random = new Random();

        String randomString = random
            .ints(leftLimit, rightLimit + 1)
            .limit(targetStringLength)
            .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
            .toString();
        return date + randomString;
    }

    @PostMapping("/orders")
    public ResponseEntity<OrderVM> createOrder(HttpSession session) throws URISyntaxException {
        OrderVM order = new OrderVM();
        order.setOrderCode(getRandomOrderCode());
        session.setAttribute(order.getOrderCode(), order);
        return ResponseEntity
            .created(new URI("/api/orders/" + order.getOrderCode()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, order.getOrderCode().toString()))
            .body(order);
    }

//    @PutMapping("/orders/{code}/{sku}")
//    public ResponseEntity<OrderVM> addProductToOrder(@PathVariable String code, @PathVariable String sku, HttpSession session) {
//        OrderVM order = (OrderVM) session.getAttribute(code);
//        order.addToCart(productRepository.getProductsBySku(sku).get());
//        return new ResponseEntity<>(order, HttpStatus.OK);
//    }

    @PutMapping("/orders/{code}/{sku}")
    public ResponseEntity<OrderVM> addProductsToOrder(@PathVariable String code, @PathVariable String sku, @RequestParam (defaultValue="1") Optional<Integer> n,HttpSession session) throws URISyntaxException {
        OrderVM order = (OrderVM) session.getAttribute(code);
        int quant = n.get();
        Product prod = productRepository.getProductsBySku(sku).get();
        boolean check = order.checkQuantity(prod, quant);
        if (check) {
            order.addToCart(prod, quant);
            return ResponseEntity
                .created(new URI("/api/orders/" + order.getOrderCode()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, order.getOrderCode().toString()))
                .body(order);
        } else {
            return new ResponseEntity<>(order, HttpStatus.FORBIDDEN);
        }
    }

    @DeleteMapping ("/orders/{orderCode}")
    public String deleteOrder(@PathVariable String orderCode, HttpSession session) {
        OrderVM order = (OrderVM) session.getAttribute(orderCode);
        if (order != null) {
            session.setAttribute(orderCode, null);
            return "Order removed!";
        } else {
            return "No order found";
        }
    }

    @GetMapping("/orders/{orderCode}")
    public ResponseEntity<OrderVM> getOrder(@PathVariable String orderCode, HttpSession session) throws URISyntaxException {
        log.debug("REST request to get Order : {}", orderCode);
        OrderVM order = (OrderVM) session.getAttribute(orderCode);
        return ResponseEntity
            .created(new URI("/api/orders/" + order.getOrderCode()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, order.getOrderCode().toString()))
            .body(order);
    }

    private Float checkDiscount(int point) {
        List<Rank> listRank = rankRepository.findAll();
        float finalDiscount = 1f;
        for (Rank rank : listRank) {
            if (rank.getAchievementPoint() > point) {
                return finalDiscount;
            }
            finalDiscount = rank.getDiscount();
        }
        return 1f;
    }


    @PutMapping("/orders/checkout/{orderCode}")
    public ResponseEntity<Order> checkoutOrder (@PathVariable String orderCode, @RequestParam Optional<String> telephone, HttpSession session) throws URISyntaxException {
        OrderVM orderVM = (OrderVM) session.getAttribute(orderCode);
        Order order = new Order();
        Customer customer = null;
        if (telephone != null) {
            if (customerRepository.findCustomerByTelephone(telephone.get()).isPresent()) {
                customer = customerRepository.findCustomerByTelephone(telephone.get()).get();
            } else {
                return new ResponseEntity<>(order, HttpStatus.FORBIDDEN);
            }
        }
        order.setCustomer(customer);
        order.setCode(orderVM.getOrderCode());
        order.setEmployee(userService.getUserWithAuthorities().get());
        order.setPayment(paymentMethodRepository.findById(1L).get());
        Order result = orderRepository.save(order);
        Map<Product, Integer> cart = orderVM.getCart();
        for (Map.Entry<Product, Integer> entry : cart.entrySet()) {
            Product prod = entry.getKey();
            OrderItem orderItem = new OrderItem();
            orderItem.setOrder(order);
            orderItem.setProduct(prod);
            orderItem.setQuantity(entry.getValue());
            float customerDiscount = 1;
            float prodDiscount = 1;
            if (customer != null) {
                customerDiscount = checkDiscount(customer.getRankPoint());
            }
            if (prod.getDiscount() != 0) {
                prodDiscount = prod.getDiscount();
            }
            orderItem.setCost(prod.getUnitPrice().subtract(prod.getUnitPrice().multiply(BigDecimal.valueOf(prodDiscount)).multiply(BigDecimal.valueOf(customerDiscount))));
            orderItem.setStatus("PAID");
            orderItemRepository.save(orderItem);
            prod.setQuantity(prod.getQuantity() - entry.getValue());
            productRepository.save(prod);
        }
        session.setAttribute(orderCode, null);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, order.getCode().toString()))
            .body(result);
    }


    /**
     * {@code PUT  /orders/:id} : Updates an existing order.
     *
     * @param id the id of the order to save.
     * @param order the order to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated order,
     * or with status {@code 400 (Bad Request)} if the order is not valid,
     * or with status {@code 500 (Internal Server Error)} if the order couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/orders/{id}")
    public ResponseEntity<Order> updateOrder(
        @PathVariable(value = "id", required = false) final String id,
        @Valid @RequestBody Order order
    ) throws URISyntaxException {
        log.debug("REST request to update Order : {}, {}", id, order);
        if (order.getCode() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, order.getCode())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Order result = orderRepository.save(order);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, order.getCode().toString()))
            .body(result);
    }

    @PostMapping("/orders/refund")
    public ResponseEntity<RefundedItem> createCustomer(@Valid @RequestBody RefundedItem refundedItem) throws URISyntaxException {
        log.debug("REST request to save Customer : {}", refundedItem);
        if (refundedItem.getId() != null) {
            throw new BadRequestAlertException("A new customer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        // refundedItem.set(userService.getUserWithAuthorities().get());
        RefundedItem result = refundedItemRepository.save(refundedItem);
        return ResponseEntity
            .created(new URI("/api/orders/refund" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /orders/:id} : Partial updates given fields of an existing order, field will ignore if it is null
     *
     * @param id the id of the order to save.
     * @param order the order to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated order,
     * or with status {@code 400 (Bad Request)} if the order is not valid,
     * or with status {@code 404 (Not Found)} if the order is not found,
     * or with status {@code 500 (Internal Server Error)} if the order couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
//    @PatchMapping(value = "/orders/{id}", consumes = "application/merge-patch+json")
//    public ResponseEntity<Order> partialUpdateOrder(
//        @PathVariable(value = "id", required = false) final String id,
//        @NotNull @RequestBody Order order
//    ) throws URISyntaxException {
//        log.debug("REST request to partial update Order partially : {}, {}", id, order);
//        if (order.getCode() == null) {
//            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
//        }
//        if (!Objects.equals(id, order.getCode())) {
//            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
//        }
//
//        if (!orderRepository.existsById(id)) {
//            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
//        }
//
//        Optional<Order> result = orderRepository
//            .findById(order.getCode())
//            .map(
//                existingOrder -> {
//                    if (order.getCode() != null) {
//                        existingOrder.setCode(order.getCode());
//                    }
//                    if (order.getCreatedAt() != null) {
//                        existingOrder.setCreatedAt(order.getCreatedAt());
//                    }
//
//                    return existingOrder;
//                }
//            )
//            .map(orderRepository::save);
//
//        return ResponseUtil.wrapOrNotFound(
//            result,
//            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, order.getCode().toString())
//        );
//    }

    /**
     * {@code GET  /orders} : get all the orders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orders in body.
     */
    @GetMapping("/orders")
    public List<Order> getAllOrders() {
        log.debug("REST request to get all Orders");
        return orderRepository.findAll();
    }

    /**
     * {@code GET  /orders/:id} : get the "id" order.
     *
     * @param id the id of the order to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the order, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/orders/{id}")
//    public ResponseEntity<Order> getOrder(@PathVariable String id) {
//        log.debug("REST request to get Order : {}", id);
//        Optional<Order> order = orderRepository.findById(id);
//        return ResponseUtil.wrapOrNotFound(order);
//    }

    /**
     * {@code DELETE  /orders/:id} : delete the "id" order.
     *
     * @param id the id of the order to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
//    @DeleteMapping("/orders/{id}")
//    public ResponseEntity<Void> deleteOrder(@PathVariable String id) {
//        log.debug("REST request to delete Order : {}", id);
//        orderRepository.deleteById(id);
//        return ResponseEntity
//            .noContent()
//            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
//            .build();
//    }
}
