package com.madcolors.web.rest;

import com.madcolors.domain.Category;
import com.madcolors.domain.Product;
import com.madcolors.repository.CategoryRepository;
import com.madcolors.repository.ProductRepository;
import com.madcolors.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.swing.text.html.parser.Entity;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.madcolors.web.rest.vm.ProductVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.madcolors.domain.Product}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductResource {

    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    private static final String ENTITY_NAME = "product";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public ProductResource(ProductRepository productRepository,
                           CategoryRepository categoryRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    /**
     * {@code POST  /products} : Create a new product.
     *
     * @param product the product to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new product, or with status {@code 400 (Bad Request)} if the product has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/products")
    public ResponseEntity<Product> createProduct(@Valid @RequestBody ProductVM product) throws URISyntaxException {
        log.debug("REST request to save Product : {}", product);

        if (productRepository.existsBySku(product.getSku())) {
            throw new BadRequestAlertException("SKU already exist", ENTITY_NAME, "skureplicate");
        }

        Product prod = new Product();
        prod.setSku(product.getSku());
        prod.setName(product.getName());
        prod.setDescription(product.getDescription());
        prod.setProdSize(product.getProdSize());
        prod.setBrand(product.getBrand());
        prod.setImageUrl(product.getImageUrl());
        prod.setGender(product.getGender());
        prod.setUnitPrice(product.getUnitPrice());
        prod.setQuantity(product.getQuantity());
        prod.setDiscount(product.getDiscount());
        Category category = categoryRepository.findByName(product.getCategory()).get();
        prod.setCategory(category);
        Product result = productRepository.save(prod);
        return ResponseEntity
            .created(new URI("/api/products/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /products/:id} : Updates an existing product.
     *
     * @param id the id of the product to save.
     * @param product the product to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated product,
     * or with status {@code 400 (Bad Request)} if the product is not valid,
     * or with status {@code 500 (Internal Server Error)} if the product couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/products/{id}")
    public ResponseEntity<Product> updateProduct(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Product product
    ) throws URISyntaxException {
        log.debug("REST request to update Product : {}, {}", id, product);
        if (product.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, product.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!productRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Product result = productRepository.save(product);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, product.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /products/:id} : Partial updates given fields of an existing product, field will ignore if it is null
     *
     * @param id the id of the product to save.
     * @param product the product to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated product,
     * or with status {@code 400 (Bad Request)} if the product is not valid,
     * or with status {@code 404 (Not Found)} if the product is not found,
     * or with status {@code 500 (Internal Server Error)} if the product couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/products")
    public ResponseEntity<Product> partialUpdateProduct(@NotNull @RequestBody ProductVM product) throws URISyntaxException {
        log.debug("REST request to partial update Product partially : {}, {}", product.getSku(), product);
        if (product.getSku() == null) {
            throw new BadRequestAlertException("Invalid sku", ENTITY_NAME, "skunull");
        }

        if (!productRepository.existsBySku(product.getSku())) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(applicationName, false, ENTITY_NAME, product.getSku(), "Product not found")).build();
        }

        Optional<Product> result = productRepository
            .getProductsBySku(product.getSku())
            .map(
                existingProduct -> {
                    if (product.getName() != null) {
                        existingProduct.setName(product.getName());
                    }
                    if (product.getDescription() != null) {
                        existingProduct.setDescription(product.getDescription());
                    }
                    if (product.getProdSize() != null) {
                        existingProduct.setProdSize(product.getProdSize());
                    }
                    if (product.getBrand() != null) {
                        existingProduct.setBrand(product.getBrand());
                    }
                    if (product.getColor() != null) {
                        existingProduct.setColor(product.getColor());
                    }
                    if (product.getImageUrl() != null) {
                        existingProduct.setImageUrl(product.getImageUrl());
                    }
                    if (product.getGender() != null) {
                        existingProduct.setGender(product.getGender());
                    }
                    if (product.getUnitPrice() != null) {
                        existingProduct.setUnitPrice(product.getUnitPrice());
                    }
                    if (product.getQuantity() != null) {
                        existingProduct.setQuantity(product.getQuantity());
                    }
                    if (product.getDiscount() != null) {
                        existingProduct.setDiscount(product.getDiscount());
                    }
                    if (product.getCategory() != null) {
                        Category category = categoryRepository.findByName(product.getCategory()).get();
                        existingProduct.setCategory(category);
                    }

                    return existingProduct;
                }
            )
            .map(productRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, product.getSku())
        );
    }

    /**
     * {@code GET  /products} : get all the products.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of products in body.
     */
    @GetMapping("/products")
    public List<Product> getAllProducts() {
        log.debug("REST request to get all Products");
        return productRepository.findAll();
    }

    /**
     * {@code GET  /products/:id} : get the "id" product.
     *
     * @param id the id of the product to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the product, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/products/{id}")
//    public ResponseEntity<Product> getProduct(@PathVariable Long id) {
//        log.debug("REST request to get Product : {}", id);
//        Optional<Product> product = productRepository.findById(id);
//        return ResponseUtil.wrapOrNotFound(product);
//    }

    @GetMapping("/products/{sku}")
    public ResponseEntity<Product> getProduct(@PathVariable String sku) {
        log.debug("REST request to get Product : {}", sku);
        Optional<Product> product = productRepository.getProductsBySku(sku);
        return ResponseUtil.wrapOrNotFound(product);
    }

    /**
     * {@code DELETE  /products/:id} : delete the "id" product.
     *
     * @param id the id of the product to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        log.debug("REST request to delete Product : {}", id);
        productRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
