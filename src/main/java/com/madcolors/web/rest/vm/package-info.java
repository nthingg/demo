/**
 * View Models used by Spring MVC REST controllers.
 */
package com.madcolors.web.rest.vm;
