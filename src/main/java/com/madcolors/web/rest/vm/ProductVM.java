package com.madcolors.web.rest.vm;

import java.math.BigDecimal;

public class ProductVM {

    private Integer id;
    private String sku;
    private String name;
    private String description;
    private String prodSize;
    private String brand;
    private String color;
    private String imageUrl;
    private String gender;
    private BigDecimal unitPrice;
    private Integer quantity;
    private Float discount;
    private String category;

    public ProductVM() {
    }

    public ProductVM(String sku, String name, String description, String prodSize, String brand, String color, String imageUrl, String gender, BigDecimal unitPrice, Integer quantity, Float discount, String category, Integer id) {
        this.id = id;
        this.sku = sku;
        this.name = name;
        this.description = description;
        this.prodSize = prodSize;
        this.brand = brand;
        this.color = color;
        this.imageUrl = imageUrl;
        this.gender = gender;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.discount = discount;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProdSize() {
        return prodSize;
    }

    public void setProdSize(String prodSize) {
        this.prodSize = prodSize;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
