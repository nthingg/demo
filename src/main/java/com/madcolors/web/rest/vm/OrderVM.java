package com.madcolors.web.rest.vm;

import com.madcolors.domain.Product;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class OrderVM implements Serializable {

    private static final long serialVersionUID = 1L;
    private String orderCode;
    private Map<Product, Integer> cart = new HashMap<>();

    public OrderVM(String orderCode, Map<Product, Integer> cart) {
        this.orderCode = orderCode;
        this.cart = cart;
    }

    public OrderVM() {
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Map<Product, Integer> getCart() {
        return cart;
    }

    public void setCart(Map<Product, Integer> cart) {
        this.cart = cart;
    }

    public void addToCart(Product product, int quantity) {
        if (cart.containsKey(product)) {
            cart.put(product, cart.get(product) + quantity);
        } else {
            cart.put(product, quantity);
        }
    }

    public boolean checkQuantity(Product product, int quantity) {
        if (product.getQuantity() < quantity) {
            return false;
        } else {
            return true;
        }
    }

    public boolean checkStock(Product prod, int quantity) {
        if (cart.containsKey(prod)) {
            if (prod.getQuantity() < quantity) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public void removeFromCart(Product product, int quantity) {
        if (cart.containsKey(product)) {
            cart.put(product, cart.get(product) - quantity);
        }
    }

}
