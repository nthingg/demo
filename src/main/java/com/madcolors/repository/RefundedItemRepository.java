package com.madcolors.repository;

import com.madcolors.domain.RefundedItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RefundedItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RefundedItemRepository extends JpaRepository<RefundedItem, Long> {}
