package com.madcolors.repository;

import com.madcolors.domain.Product;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data SQL repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> getProductsBySku(String productSKU);
    boolean existsBySku (String sku);
}
