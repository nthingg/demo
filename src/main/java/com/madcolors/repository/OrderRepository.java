package com.madcolors.repository;

import com.madcolors.domain.Order;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Order entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderRepository extends JpaRepository<Order, String> {
    @Query("select jhiOrder from Order jhiOrder where jhiOrder.employee.login = ?#{principal.username}")
    List<Order> findByEmployeeIsCurrentUser();
}
