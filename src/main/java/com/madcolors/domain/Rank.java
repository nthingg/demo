package com.madcolors.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Rank.
 */
@Entity
@Table(name = "ranks")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Rank implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "rank_name", length = 20, nullable = false, unique = true)
    private String name;

    @NotNull
    @Column(name = "achievement_point", nullable = false)
    private Integer achievementPoint;

    @NotNull
    @Column(name = "rank_discount", nullable = false)
    private Float discount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Rank id(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public Rank name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAchievementPoint() {
        return this.achievementPoint;
    }

    public Rank achievementPoint(Integer achievementPoint) {
        this.achievementPoint = achievementPoint;
        return this;
    }

    public void setAchievementPoint(Integer achievementPoint) {
        this.achievementPoint = achievementPoint;
    }

    public Float getDiscount() {
        return this.discount;
    }

    public Rank discount(Float discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rank)) {
            return false;
        }
        return id != null && id.equals(((Rank) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Rank{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", achievementPoint=" + getAchievementPoint() +
            ", discount=" + getDiscount() +
            "}";
    }
}
