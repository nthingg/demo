package com.madcolors.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A RefundedItem.
 */
@Entity
@Table(name = "refunded_items")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RefundedItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 250)
    @Column(name = "refund_reason", length = 250)
    private String reason;

    @NotNull
    @Column(name = "refund_price", precision = 21, scale = 2, nullable = false)
    private BigDecimal refundPrice;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @ManyToOne
    @JsonIgnoreProperties(value = { "order", "product" }, allowSetters = true)
    @JoinColumn(name = "order_item_id")
    private OrderItem orderItem;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RefundedItem id(Long id) {
        this.id = id;
        return this;
    }

    public String getReason() {
        return this.reason;
    }

    public RefundedItem reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getRefundPrice() {
        return this.refundPrice;
    }

    public RefundedItem refundPrice(BigDecimal refundPrice) {
        this.refundPrice = refundPrice;
        return this;
    }

    public void setRefundPrice(BigDecimal refundPrice) {
        this.refundPrice = refundPrice;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public RefundedItem createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public OrderItem getOrderItem() {
        return this.orderItem;
    }

    public RefundedItem orderItem(OrderItem orderItem) {
        this.setOrderItem(orderItem);
        return this;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RefundedItem)) {
            return false;
        }
        return id != null && id.equals(((RefundedItem) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RefundedItem{" +
            "id=" + getId() +
            ", reason='" + getReason() + "'" +
            ", refundPrice=" + getRefundPrice() +
            ", createdAt='" + getCreatedAt() + "'" +
            "}";
    }
}
