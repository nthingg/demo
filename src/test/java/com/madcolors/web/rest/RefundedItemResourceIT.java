package com.madcolors.web.rest;

import static com.madcolors.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.madcolors.IntegrationTest;
import com.madcolors.domain.RefundedItem;
import com.madcolors.repository.RefundedItemRepository;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RefundedItemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RefundedItemResourceIT {

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_REFUND_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_REFUND_PRICE = new BigDecimal(2);

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/refunded-items";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RefundedItemRepository refundedItemRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRefundedItemMockMvc;

    private RefundedItem refundedItem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefundedItem createEntity(EntityManager em) {
        RefundedItem refundedItem = new RefundedItem()
            .reason(DEFAULT_REASON)
            .refundPrice(DEFAULT_REFUND_PRICE)
            .createdAt(DEFAULT_CREATED_AT);
        return refundedItem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RefundedItem createUpdatedEntity(EntityManager em) {
        RefundedItem refundedItem = new RefundedItem()
            .reason(UPDATED_REASON)
            .refundPrice(UPDATED_REFUND_PRICE)
            .createdAt(UPDATED_CREATED_AT);
        return refundedItem;
    }

    @BeforeEach
    public void initTest() {
        refundedItem = createEntity(em);
    }

    @Test
    @Transactional
    void createRefundedItem() throws Exception {
        int databaseSizeBeforeCreate = refundedItemRepository.findAll().size();
        // Create the RefundedItem
        restRefundedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refundedItem)))
            .andExpect(status().isCreated());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeCreate + 1);
        RefundedItem testRefundedItem = refundedItemList.get(refundedItemList.size() - 1);
        assertThat(testRefundedItem.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testRefundedItem.getRefundPrice()).isEqualByComparingTo(DEFAULT_REFUND_PRICE);
        assertThat(testRefundedItem.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
    }

    @Test
    @Transactional
    void createRefundedItemWithExistingId() throws Exception {
        // Create the RefundedItem with an existing ID
        refundedItem.setId(1L);

        int databaseSizeBeforeCreate = refundedItemRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRefundedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refundedItem)))
            .andExpect(status().isBadRequest());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkRefundPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = refundedItemRepository.findAll().size();
        // set the field null
        refundedItem.setRefundPrice(null);

        // Create the RefundedItem, which fails.

        restRefundedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refundedItem)))
            .andExpect(status().isBadRequest());

        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = refundedItemRepository.findAll().size();
        // set the field null
        refundedItem.setCreatedAt(null);

        // Create the RefundedItem, which fails.

        restRefundedItemMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refundedItem)))
            .andExpect(status().isBadRequest());

        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRefundedItems() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        // Get all the refundedItemList
        restRefundedItemMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(refundedItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].refundPrice").value(hasItem(sameNumber(DEFAULT_REFUND_PRICE))))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())));
    }

    @Test
    @Transactional
    void getRefundedItem() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        // Get the refundedItem
        restRefundedItemMockMvc
            .perform(get(ENTITY_API_URL_ID, refundedItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(refundedItem.getId().intValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.refundPrice").value(sameNumber(DEFAULT_REFUND_PRICE)))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()));
    }

    @Test
    @Transactional
    void getNonExistingRefundedItem() throws Exception {
        // Get the refundedItem
        restRefundedItemMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRefundedItem() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();

        // Update the refundedItem
        RefundedItem updatedRefundedItem = refundedItemRepository.findById(refundedItem.getId()).get();
        // Disconnect from session so that the updates on updatedRefundedItem are not directly saved in db
        em.detach(updatedRefundedItem);
        updatedRefundedItem.reason(UPDATED_REASON).refundPrice(UPDATED_REFUND_PRICE).createdAt(UPDATED_CREATED_AT);

        restRefundedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRefundedItem.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRefundedItem))
            )
            .andExpect(status().isOk());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
        RefundedItem testRefundedItem = refundedItemList.get(refundedItemList.size() - 1);
        assertThat(testRefundedItem.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testRefundedItem.getRefundPrice()).isEqualTo(UPDATED_REFUND_PRICE);
        assertThat(testRefundedItem.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void putNonExistingRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, refundedItem.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refundedItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(refundedItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(refundedItem)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRefundedItemWithPatch() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();

        // Update the refundedItem using partial update
        RefundedItem partialUpdatedRefundedItem = new RefundedItem();
        partialUpdatedRefundedItem.setId(refundedItem.getId());

        partialUpdatedRefundedItem.refundPrice(UPDATED_REFUND_PRICE).createdAt(UPDATED_CREATED_AT);

        restRefundedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefundedItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefundedItem))
            )
            .andExpect(status().isOk());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
        RefundedItem testRefundedItem = refundedItemList.get(refundedItemList.size() - 1);
        assertThat(testRefundedItem.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testRefundedItem.getRefundPrice()).isEqualByComparingTo(UPDATED_REFUND_PRICE);
        assertThat(testRefundedItem.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void fullUpdateRefundedItemWithPatch() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();

        // Update the refundedItem using partial update
        RefundedItem partialUpdatedRefundedItem = new RefundedItem();
        partialUpdatedRefundedItem.setId(refundedItem.getId());

        partialUpdatedRefundedItem.reason(UPDATED_REASON).refundPrice(UPDATED_REFUND_PRICE).createdAt(UPDATED_CREATED_AT);

        restRefundedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRefundedItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRefundedItem))
            )
            .andExpect(status().isOk());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
        RefundedItem testRefundedItem = refundedItemList.get(refundedItemList.size() - 1);
        assertThat(testRefundedItem.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testRefundedItem.getRefundPrice()).isEqualByComparingTo(UPDATED_REFUND_PRICE);
        assertThat(testRefundedItem.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void patchNonExistingRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, refundedItem.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refundedItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(refundedItem))
            )
            .andExpect(status().isBadRequest());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRefundedItem() throws Exception {
        int databaseSizeBeforeUpdate = refundedItemRepository.findAll().size();
        refundedItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRefundedItemMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(refundedItem))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RefundedItem in the database
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRefundedItem() throws Exception {
        // Initialize the database
        refundedItemRepository.saveAndFlush(refundedItem);

        int databaseSizeBeforeDelete = refundedItemRepository.findAll().size();

        // Delete the refundedItem
        restRefundedItemMockMvc
            .perform(delete(ENTITY_API_URL_ID, refundedItem.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RefundedItem> refundedItemList = refundedItemRepository.findAll();
        assertThat(refundedItemList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
