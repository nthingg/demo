package com.madcolors.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.madcolors.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RefundedItemTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RefundedItem.class);
        RefundedItem refundedItem1 = new RefundedItem();
        refundedItem1.setId(1L);
        RefundedItem refundedItem2 = new RefundedItem();
        refundedItem2.setId(refundedItem1.getId());
        assertThat(refundedItem1).isEqualTo(refundedItem2);
        refundedItem2.setId(2L);
        assertThat(refundedItem1).isNotEqualTo(refundedItem2);
        refundedItem1.setId(null);
        assertThat(refundedItem1).isNotEqualTo(refundedItem2);
    }
}
