package com.madcolors.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.madcolors.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Order.class);
        Order order1 = new Order();
        order1.setCode("1L");
        Order order2 = new Order();
        order2.setCode(order1.getCode());
        assertThat(order1).isEqualTo(order2);
        order2.setCode("2L");
        assertThat(order1).isNotEqualTo(order2);
        order1.setCode(null);
        assertThat(order1).isNotEqualTo(order2);
    }
}
