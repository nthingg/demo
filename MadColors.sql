CREATE DATABASE  IF NOT EXISTS `madcolors` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `madcolors`;
-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: madcolors
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorities` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES ('ROLE_ADMIN'),('ROLE_CASHIER'),('ROLE_MANAGER'),('ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cate_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Áo phông'),(2,'Áo sơ mi'),(3,'Áo khoác'),(4,'Áo thun'),(5,'Quần shorts'),(6,'Quần jeans'),(7,'Quần dài'),(8,'Quần jogger'),(9,'Quần tây'),(10,'Váy xoè'),(11,'Váy liền'),(12,'Váy suông');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `telephone` varchar(15) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `rank_point` int NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `telephone` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'0977788890','Nguyễn Hồng Hiệp','honghiep@gmail.com','97 Hoàng Hoa Thám, phường Bến Nghé, Quận 1, Tp HCM',6473,'2020-05-14 09:35:54'),(2,'0977788891','Thái Văn Luật','vanluat@gmail.com','38 Hoàng Trọng Mậu, Phường Tân Hưng, Quận 7, Tp HCM',2437,'2021-12-27 14:25:28'),(3,'0977788892','Đào Xuân Giang','xuangiang@gmail.com','82/47 Đinh Bộ Lĩnh, Phường 26, Quận Bình Thạnh, Tp HCM',9786,'2022-04-13 07:46:29'),(4,'0977788893','Phạm Xuân Quyết','xuanquyet@gmail.com','11 Nguyễn Huy Tưởng, Phường 6, Quận Bình Thạnh, Tp HCM',12563,'2023-01-30 08:27:36');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2023-01-31 19:03:20',1,'MARK_RAN','8:ddbdb9df09c2009472b8c5c987ef4187','createTable tableName=employees; createTable tableName=authorities; createTable tableName=employee_authority; addPrimaryKey tableName=employee_authority; addForeignKeyConstraint baseTableName=employee_authority, constraintName=fk_authority_name, r...','',NULL,'4.3.5',NULL,NULL,'5166600059'),('20230131102349-1','jhipster','config/liquibase/changelog/20230131102349_added_entity_Rank.xml','2023-01-31 19:04:06',2,'MARK_RAN','8:3211d6215b205cc1c58fc0051d805aa4','createTable tableName=ranks','',NULL,'4.3.5',NULL,NULL,'5166646404'),('20230131102350-1','jhipster','config/liquibase/changelog/20230131102350_added_entity_Customer.xml','2023-01-31 19:05:43',3,'MARK_RAN','8:a2a108b0f9b59a152004bfc2856995fc','createTable tableName=customers; dropDefaultValue columnName=created_at, tableName=customers','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102351-1','jhipster','config/liquibase/changelog/20230131102351_added_entity_Category.xml','2023-01-31 19:05:43',4,'MARK_RAN','8:02af88977862e134b4bad5d319e13cf4','createTable tableName=categories','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102352-1','jhipster','config/liquibase/changelog/20230131102352_added_entity_Product.xml','2023-01-31 19:05:43',5,'MARK_RAN','8:d564ed5cb093f97e8f53364b59f2ba6b','createTable tableName=products','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102353-1','jhipster','config/liquibase/changelog/20230131102353_added_entity_PaymentMethod.xml','2023-01-31 19:05:43',6,'MARK_RAN','8:f6038a5a761acff251417c377479b30d','createTable tableName=payment_methods','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102354-1','jhipster','config/liquibase/changelog/20230131102354_added_entity_Order.xml','2023-01-31 19:05:43',7,'MARK_RAN','8:e3ade7d77db726fd7ca080239764b359','createTable tableName=orders; dropDefaultValue columnName=created_at, tableName=orders','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102355-1','jhipster','config/liquibase/changelog/20230131102355_added_entity_OrderItem.xml','2023-01-31 19:05:43',8,'MARK_RAN','8:e36c8ce91b88135bb658607e4087eb86','createTable tableName=order_items','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102356-1','jhipster','config/liquibase/changelog/20230131102356_added_entity_RefundedItem.xml','2023-01-31 19:05:43',9,'MARK_RAN','8:2a4ec7a2780eebb2de773e57835782d0','createTable tableName=refunded_items; dropDefaultValue columnName=created_at, tableName=refunded_items','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102352-2','jhipster','config/liquibase/changelog/20230131102352_added_entity_constraints_Product.xml','2023-01-31 19:05:43',10,'EXECUTED','8:1bd2b3a1da86d0f0d2001151403e220b','addForeignKeyConstraint baseTableName=products, constraintName=fk_products__cate_id, referencedTableName=categories','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102354-2','jhipster','config/liquibase/changelog/20230131102354_added_entity_constraints_Order.xml','2023-01-31 19:05:43',11,'EXECUTED','8:6b520ac8a9dab258e5bde23330095a89','addForeignKeyConstraint baseTableName=orders, constraintName=fk_orders__customer_id, referencedTableName=customers; addForeignKeyConstraint baseTableName=orders, constraintName=fk_orders__emp_id, referencedTableName=employees; addForeignKeyConstra...','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102355-2','jhipster','config/liquibase/changelog/20230131102355_added_entity_constraints_OrderItem.xml','2023-01-31 19:05:44',12,'EXECUTED','8:b57269170b6eaa932cb72dfc66fa0e9a','addForeignKeyConstraint baseTableName=order_items, constraintName=fk_order_items__order_code, referencedTableName=orders; addForeignKeyConstraint baseTableName=order_items, constraintName=fk_order_items__pro_id, referencedTableName=products','',NULL,'4.3.5',NULL,NULL,'5166743329'),('20230131102356-2','jhipster','config/liquibase/changelog/20230131102356_added_entity_constraints_RefundedItem.xml','2023-01-31 19:05:44',13,'EXECUTED','8:c91da931c6c09867c837a59f50d72f96','addForeignKeyConstraint baseTableName=refunded_items, constraintName=fk_refunded_items__order_item_id, referencedTableName=order_items','',NULL,'4.3.5',NULL,NULL,'5166743329');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `databasechangeloglock` (
  `ID` int NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,_binary '\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_authority`
--

DROP TABLE IF EXISTS `employee_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_authority` (
  `emp_id` bigint NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`emp_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `authorities` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_authority`
--

LOCK TABLES `employee_authority` WRITE;
/*!40000 ALTER TABLE `employee_authority` DISABLE KEYS */;
INSERT INTO `employee_authority` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_CASHIER'),(1,'ROLE_USER'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `employee_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(10) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_employee_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (1,'admin','$2a$10$clChO8HQ69JlBr/wCZG5zuzEzaHOuNOYSWbg7OAT/3nOO.QITyeHK','Nguyễn Hồng Hiệp','0967462176','honghiepnguyen27@gmail.com','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(2,'cashier1','$2a$10$oFEDq6uusor8p2CPQqRtQu0jPxKECLpNPUcz529EwRSqPFPeZl1jC','Đào Xuân Giang','0987526477','xuangiang@gmail.com','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(3,'cashier2','$2a$10$0c2Y65BqmguxQbq4.t.rKuwJhQm/LBJqqcmnotKS2zxQNYinv.2Ee','Đinh Ngọc Huyền','0965466278','ngochuyen@gmail.com','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(4,'manager','$2a$10$EDj/x.uBupuMGIEMRcRHFOwJSzULoJSqF4YmxgNPbqWG2OmriHDeG','Phạm Thanh Tùng','0967355267','thanhtung@gmail.com','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL),(5,'cashier3','$2a$10$Shfw9vhjvd9jXn2wD2PyeOITTDk02raBZg3/vhpC37TMOWO5O7VjG','Tống Ngọc Trâm Anh','0962547826','tramanh@gmail.com','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_code` varchar(20) NOT NULL,
  `prod_id` int NOT NULL,
  `quantity` int NOT NULL,
  `cost` decimal(10,2) NOT NULL,
  `item_status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_code` (`order_code`),
  KEY `prod_id` (`prod_id`),
  CONSTRAINT `fk_order_items__order_code` FOREIGN KEY (`order_code`) REFERENCES `orders` (`order_code`),
  CONSTRAINT `fk_order_items__pro_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`),
  CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_code`) REFERENCES `orders` (`order_code`),
  CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_items`
--

LOCK TABLES `order_items` WRITE;
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `order_code` varchar(20) NOT NULL,
  `customer_id` int DEFAULT NULL,
  `emp_id` bigint NOT NULL,
  `payment_id` int NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`order_code`),
  KEY `customer_id` (`customer_id`),
  KEY `emp_id` (`emp_id`),
  KEY `payment_id` (`payment_id`),
  CONSTRAINT `fk_orders__customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `fk_orders__emp_id` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `fk_orders__payment_id` FOREIGN KEY (`payment_id`) REFERENCES `payment_methods` (`id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`payment_id`) REFERENCES `payment_methods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_methods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `payment_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_methods`
--

LOCK TABLES `payment_methods` WRITE;
/*!40000 ALTER TABLE `payment_methods` DISABLE KEYS */;
INSERT INTO `payment_methods` VALUES (1,'Cash');
/*!40000 ALTER TABLE `payment_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `sku` varchar(15) NOT NULL,
  `prod_name` varchar(100) NOT NULL,
  `cate_id` int NOT NULL,
  `prod_desc` varchar(250) DEFAULT NULL,
  `size` varchar(5) DEFAULT NULL,
  `brand` varchar(30) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `quantity` int NOT NULL,
  `prod_discount` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sku` (`sku`),
  KEY `cate_id` (`cate_id`),
  CONSTRAINT `fk_products__cate_id` FOREIGN KEY (`cate_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`cate_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'H070054','Váy Adidas Always Original Laced Strap Dress',12,'Được thiết kế kiểu dáng trẻ trung, hiện đại đến từ thương hiệu Adidas nổi tiếng. Váy được làm từ vải cao cấp mang lại cảm giác thoải mái cho người mặc.','XXS','Adidas','Xanh blue','https://cdn.madcolors.com/images/adidas-always-original-laced-strap-dress.png','Nữ',1700000.00,46,0.00),(2,'SH1559','Áo Khoác Lacoste Sport Regular Fit 12',3,'Lacoste cấp phép nhãn hàng của mình cho nhiều công ty khác nhau. Mặc dù Lacoste Polo Shirts cũng đồng thời được cấp giấy phép ở Thái Lan bởi ICC và cả Trung Quốc.','XS','Lacoste','Xám','https://cdn.madcolors.com/images/ao-khoac-lacoste-sport-regular-fit-12.png','Nam',3300000.00,24,0.02),(3,'DM09551','Quần Jeans Tommy Hilfiger Straight 37',6,'Tommy Hilfiger mang đến phong cách thời trang cao cấp, chất lượng và giá trị cho người tiêu dùng trên toàn thế giới dưới 2 nhãn hiệu Tommy Hilfiger và Hilfiger Denim.','XL','Tommy','Xanh','https://cdn.madcolors.com/images/quan-jeans-tommy-hilfiger-straight-37.png','Nam',2200000.00,37,0.05),(4,'7M85166','Áo Thun Lucky Brand Regular Fit 11',4,'Gene & Barry đã tạo ra một thương hiệu nổi tiếng với sự chú ý đến từng chi tiết được đưa vào mỗi chiếc quần jeans.','M','Lucky Brand','Xanh Dương','https://cdn.madcolors.com/images/ao-thun-lucky-brand-regular-fit-11.png','Nam',900000.00,126,0.00),(5,'CH49764','Áo Sơ Mi Lacoste Regular Fit 39',2,'Lacoste cấp phép nhãn hàng của mình cho nhiều công ty khác nhau. Gần đây nhất Devanlay đã sở hữu giấy phép độc quyền kinh doanh quần áo trên toàn thế giới của hãng.','M','Lacoste','Trắng','https://cdn.madcolors.com/images/ao-so-mi-lacoste-regular-fit-39.png','Nam',2400000.00,57,0.02),(6,'H063814','Quần Jogger MLB Logo New York Yankees',8,'Là sản phẩm đến từ thương hiệu MLB nổi tiếng của Hàn Quốc. Sở hữu gam màu trắng trang nhã cùng thiết kế đơn giản, chiếc quần jogger MLB mang lại cảm giác thoải mái cho người mặc.','XS','MLB','Trắng','https://cdn.madcolors.com/images/quan-jogger-mlb-logo-new-york-yankees.png','Unisex',2180000.00,12,0.01),(7,'H071228','Quần Shorts MLB Women Colorblock',5,'Là sản phẩm đến từ thương hiệu MLB nổi tiếng của Hàn Quốc. Mang nét trẻ trung và năng động, mẫu quần đã trở thành item không thể thiếu trong tủ đồ ngày hè.','XS','MLB','Xanh dương','https://cdn.madcolors.com/images/quan-shorts-mlb-women-s-colorblock.png','Nữ',1870000.00,45,0.00),(8,'H059309','Váy Dolce & Gabbana Floral',10,'Là sản phẩm đến từ thương hiệu Dolce & Gabbana nổi tiếng của Ý. Váy được làm từ chất liệu cao cấp nên rất mềm mịn, mang lại cảm giác thoải mái cho người mặc.','M','Dolce & Gabbana','Phối màu','https://cdn.madcolors.com/images/vay-dolce-gabbana-floral.png','Nữ',12400000.00,12,0.05);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ranks`
--

DROP TABLE IF EXISTS `ranks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ranks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `rank_name` varchar(20) NOT NULL,
  `achievement_point` int NOT NULL,
  `rank_discount` decimal(3,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rank_name` (`rank_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ranks`
--

LOCK TABLES `ranks` WRITE;
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` VALUES (1,'BRONZE',0,0.00),(2,'SILVER',1000,0.03),(3,'GOLD',5000,0.05),(4,'PLATINUM',7000,0.07),(5,'DIAMOND',10000,0.10);
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refunded_items`
--

DROP TABLE IF EXISTS `refunded_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `refunded_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `refund_reason` varchar(250) DEFAULT NULL,
  `emp_id` bigint NOT NULL,
  `refund_price` decimal(10,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_item_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `emp_id` (`emp_id`),
  KEY `refunded_items_ibfk_2_idx` (`order_item_id`),
  CONSTRAINT `fk_refunded_items__order_item_id` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`),
  CONSTRAINT `refunded_items_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `refunded_items_ibfk_2` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refunded_items`
--

LOCK TABLES `refunded_items` WRITE;
/*!40000 ALTER TABLE `refunded_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `refunded_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-31 20:11:22
